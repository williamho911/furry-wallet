import express, { Request, Response } from 'express';
import { client } from './main';

export const walletRoutes = express.Router();

walletRoutes.get("/page", getTransactions) // redundant
walletRoutes.post("/submit/:index", postTransaction)
walletRoutes.delete("/submit/:index", deleteTransaction)
walletRoutes.get('/categories', getCategories)
walletRoutes.get('/pet', getPet)
walletRoutes.put('/submit/:index', putTransaction)
walletRoutes.get('/info', getInfo)
walletRoutes.get('/buy', postProps)
walletRoutes.get('/money/:index', getMoney)
walletRoutes.get('/bag', getProps)
walletRoutes.get('/feed', getFeed)

//Get user info
async function getInfo(req: Request, res: Response) {
    let result = await client.query(`SELECT username,coins FROM users WHERE id = $1`, [req.session?.user.id])
    res.json(result.rows)
}

// redundant
//Get data transactions inner join table categories
// Restful Web Service <-- Use by machine, not human
async function getTransactions(req: Request, res: Response) {
    let result = await client.query(`SELECT * FROM categories INNER JOIN transactions ON categories.id = category_id WHERE user_id = $1 ORDER BY date_created DESC;`, [parseInt(req.session?.user.id)])
    // let result = await client.query(`SELECT image,name,sum(amount) FROM categories INNER JOIN transactions ON categories.id = category_id group by category_id, name, image ORDER BY sum DESC;`)
    res.json(result.rows)
}

//Submit Transaction
async function postTransaction(req: Request, res: Response) {
    if (parseInt(req.params.index) == 12) { // sure die , need to store is_expense in category table
        await client.query(`insert into transactions (category_id,user_id, transaction_date, amount, is_expense, date_created) VALUES ($1, $4, $2, $3, 'f', NOW());`, [req.params.index, req.body.date, parseInt(req.body.content) + .00, parseInt(req.session?.user.id)])
    } else {
        await client.query(`insert into transactions (category_id,user_id, transaction_date, amount, is_expense, date_created) VALUES ($1, $4, $2, $3, 't', NOW());`, [req.params.index, req.body.date, parseInt(req.body.content) + .00, parseInt(req.session?.user.id)])
    }
    const pets = await client.query(`SELECT * FROM adoption WHERE user_id = $1 ORDER BY is_alive DESC`, [req.session?.user.id])
    const pet = pets.rows[0]
    // let petid = petCondition.id
    // let hp = petCondition.hp
    // let hunger = petCondition.hungry
    let alive = pet.is_alive
    const user = await client.query(`SELECT * FROM users WHERE id = $1`, [req.session?.user.id])
    let coins = user.rows[0].coins
    if (alive) {
        // if (hunger > 0) {
        //     hunger = 0
        //     await client.query(`UPDATE adoption SET hungry = $1, condition_update = NOW() WHERE user_id = $2 and id=$3`, [hunger, req.session?.user.id, petid])
        //     if (hp < 100) {
        //         hp = 100
        //         await client.query(`UPDATE adoption SET hp = $1, condition_update = NOW() WHERE user_id = $2 and id=$3`, [hp, req.session?.user.id, petid])
        //     }
        // }
        coins += 10
        await client.query(`UPDATE users set coins = coins + 10 where id  = $1`,[req.session?.user.id]);
        // await client.query(`UPDATE users SET coins = ${coins} WHERE id = ${req.session?.user.id}`)
    }
    res.redirect('/')
    // res.json({success:true})
}

//Delete transaction
async function deleteTransaction(req: Request, res: Response) {
    let result = parseInt(req.params.index)
    await client.query(`DELETE FROM transactions WHERE id = $1;`, [result])
    res.json("deleted")// need a real json object
}

//Get data by date
async function getCategories(req: Request, res: Response) {
    let date = req.query.date
    let result = await client.query(`SELECT * FROM categories INNER JOIN transactions  ON categories.id = transactions.category_id WHERE DATE(transaction_date) = $1 AND user_id = $2 ORDER BY categories.id ASC;`, [date, parseInt(req.session?.user.id)])
    res.json(result.rows)
}

//Get pet data and health status
async function getPet(req: Request, res: Response) {
    let data = await client.query(`SELECT *, adoption.id as adoption FROM adoption LEFT JOIN transactions ON adoption.user_id = transactions.user_id WHERE adoption.user_id = $1 ORDER BY is_alive DESC, born_date DESC;`, [parseInt(req.session?.user.id)])
    let petData = data.rows[0]
    console.log('Pet data: ', petData)
    let hp = petData.hp
    let hunger = petData.hungry
    let adoptionId = petData.adoption;
    let alive = petData.is_alive
    // const {hp,hungry, adoption,alive} = petData;

    let lastTransactions: Date = new Date(petData.date_created)
    let lastUpdate: Date = new Date(petData.condition_update) // updated_at
    let today: Date = new Date()
    let diff = 0
    const user = await client.query(`SELECT * FROM users WHERE id = $1`, [req.session?.user.id])
    let coins = user.rows[0].coins

    if (hp > 0) {
        //Get seconds of time difference
        if (lastTransactions == null) {
            diff = Math.floor((today.getTime() - petData.born_date.getTime()) / 1000)
        } else if (lastTransactions && lastUpdate > lastTransactions) {
            diff = Math.floor((today.getTime() - lastUpdate.getTime()) / 1000)
        } else {// redundant
            diff = Math.floor((today.getTime() - lastTransactions.getTime()) / 1000)
        }

        //Get hp and hunger deduction round 
        let round = Math.floor(diff / 3600) //Unit in SEC
        if (round > 25) {
            round = 25
        }

        for (let i = 0; i < round; i++) {
            if (hunger <= 90) {
                hunger += 10
                await client.query(`UPDATE adoption SET hungry = $1, condition_update = NOW() WHERE user_id = $2 AND id = $3`, [hunger, req.session?.user.id, adoptionId])
            } else {
                await client.query(`UPDATE adoption SET hungry = $1, condition_update = NOW() WHERE user_id = $2 AND id = $3`, [hunger, req.session?.user.id, adoptionId])
                if (hp > 10) {
                    hp -= 10
                    // Use update sql to do it
                    await client.query(`UPDATE adoption SET hp = $1, hungry = $3, condition_update = NOW() WHERE user_id = $2 AND id = $4`, [hp, req.session?.user.id, hunger, adoptionId])
                } else {
                    hp = 0
                    if (alive && hp == 0) {
                        coins -= 100
                        alive = false
                        // sql injection issue
                        await client.query(`UPDATE users SET coins = ${coins} WHERE id = ${req.session?.user.id}`)
                        await client.query(`UPDATE adoption SET hp = $1, hungry = $3, condition_update = NOW(), is_alive = 'f' WHERE user_id = $2 AND id = $4`, [hp, req.session?.user.id, hunger, adoptionId])
                    }
                }
            }
        }
    }

    let result = await client.query(`SELECT * FROM adoption WHERE user_id = $1 ORDER BY is_alive DESC, id DESC`, [parseInt(req.session?.user.id)])
    res.json(result.rows)
    // if (diff > 1 && hp >= 10) {
    //     hp -= 10
    //     await client.query(`UPDATE adoption SET hp = $1 WHERE user_id = $2`, [hp, req.session?.user.id])
    // } else if (diff > 10 && hp <= 0) {
    //     hp = 0
    //     await client.query(`UPDATE adoption SET hp = $1 WHERE user_id = $2`, [hp, req.session?.user.id])
    // }
}

//Edit data
async function putTransaction(req: Request, res: Response) {
    await client.query('UPDATE transactions SET amount = $1 WHERE id = $2', [req.body.value, parseInt(req.params.index)])
    res.json('ok')// responds json
}

//Buy prop and insert into usersDB
async function postProps(req: Request, res: Response) {
    let props: any = req.query.prop
    // SELECT id from props , shouldn't hard code
    if (parseInt(props) == 1) {
        await client.query(`INSERT INTO user_prop (user_id, prop_id, date_created) VALUES ($1, 4, NOW())`, [req.session?.user.id])
    } else if (parseInt(props) == 2) {
        await client.query(`INSERT INTO user_prop (user_id, prop_id, date_created) VALUES ($1, 3, NOW())`, [req.session?.user.id])
    } else {
        await client.query(`INSERT INTO user_prop (user_id, prop_id, date_created) VALUES ($1, 2, NOW())`, [req.session?.user.id])
    }
    // Should also deduct money
    // transactions
    res.json("done")
}

//Get users coins value
async function getMoney(req: Request, res: Response) {
    let money = req.params.index
    await client.query(`UPDATE users SET coins = $1 WHERE id = $2`, [parseInt(money), req.session?.user.id])
    res.json('done')
}

//Get users props item
async function getProps(req: Request, res: Response) {
    const result = await client.query(`SELECT user_id, prop_id, COUNT (prop_id) from user_prop WHERE user_id = $1 GROUP BY prop_id, user_id ORDER BY prop_id DESC;`, [req.session?.user.id])
    res.json(result.rows)
}

//Use props to feed pet
async function getFeed(req: Request, res: Response) {
    const petCondition = await client.query(`SELECT * FROM adoption where user_id = $1 AND is_alive = 't'`, [req.session?.user.id])
    let hp = petCondition.rows[0].hp
    let hunger = petCondition.rows[0].hungry
    const petID = petCondition.rows[0].id
    let item: any = req.query.item
    // Design your logic around data, not around code
    /*
     *   health_increment
     *   hunger_increment
     *   '+10' -> add 10 
     *   '-10' -> minus 10
     *   '+MAX' -> add to max health 
     */
    if (petCondition.rows[0].is_alive) {
        if (parseInt(item) == 4) {
            let stocks = await client.query(`SELECT * FROM user_prop WHERE user_id = $1 AND prop_id = $2 ORDER BY date_created ASC;`, [req.session?.user.id, 4])
            await client.query(`DELETE FROM user_prop WHERE id = $1;`, [stocks.rows[0].id])
            if (hp >= 80) {
                hp = 100
            } else {
                hp += 20
            }
            await client.query(`UPDATE adoption SET hp = ${hp}, condition_update = NOW() WHERE id = $1`, [petID])
        } else if (parseInt(item) == 3) {
            let stocks = await client.query(`SELECT * FROM user_prop WHERE user_id = $1 AND prop_id = $2 ORDER BY date_created ASC;`, [req.session?.user.id, 3])
            await client.query(`DELETE FROM user_prop WHERE id = $1;`, [stocks.rows[0].id])
            if (hunger > 0) {
                hunger = 0
            } else if (hunger == 0) {
                if (hp < 50) {
                    hp += 50
                } else {
                    hp = 100
                }
            }
            await client.query(`UPDATE adoption SET hp = ${hp}, hungry = ${hunger}, condition_update = NOW() WHERE id = $1`, [petID])
        } else if (parseInt(item) == 2) {
            let stocks = await client.query(`SELECT * FROM user_prop WHERE user_id = $1 AND prop_id = $2 ORDER BY date_created ASC;`, [req.session?.user.id, 2])
            await client.query(`DELETE FROM user_prop WHERE id = $1;`, [stocks.rows[0].id])
            if (hp < 100 || hunger > 0) {
                hp = 100
                hunger = 0
            }
            await client.query(`UPDATE adoption SET hp = ${hp}, hungry = ${hunger}, condition_update = NOW() WHERE id = $1`, [petID])
        }
    }
    res.json('done')// need json object
}