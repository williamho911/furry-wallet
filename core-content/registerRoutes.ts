import express from 'express';
import { client } from './main';
import { hashPassword, checkPassword } from './hashRoutes';
import { verifyEmail } from './verifyEmailRoutes';
import { v4 as uuidv4 } from 'uuid';

export const registerRoutes = express.Router();

// register post request handler

registerRoutes.post('/reset', async(req, res)=>{
  let code = req.body?.resetCode;
  let password = req.body?.passwordField
  let hashPw = await hashPassword(password);
  // Password repeat field does not need to be on backend
  let samePw = req.body?.passwordField === req.body?.passwordRepeatField ? true:false;
  let result = await client.query(`SELECT * FROM users where reset_code = $1`, [code])
  if(!result.rows[0]||!samePw){
    res.redirect('/?verify=Something went wrong, please reset password again')
    return
  }
  if(result){
    await client.query(`UPDATE users SET password = $1 WHERE reset_code = $2`, [hashPw, code])
    await client.query(`UPDATE users SET reset_code = null WHERE reset_code = $1`,[code])
}
  res.redirect('/?verify=Password reset successfully, please login')
})

registerRoutes.post('/forget', async(req, res)=>{
  // console.log(123)
  let email = req.body.emailField;
  let emailError = "";
  let regEmail = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
  if (!regEmail.test(email)) {
    emailError = "Email is not valid";
    res.redirect(`/forgetpassword.html?emailFieldError=${emailError}`)
    return
  }
  let emailResult = await client.query(`SELECT * FROM users where email = $1`, [email])
  // console.log(emailResult.rows[0])
  if(!emailResult.rows[0]){
    emailError = "No such a user";
    res.redirect(`/forgetpassword.html?emailFieldError=${emailError}`)
    return
  }
  let resetCode = uuidv4();
  verifyEmail(email, resetCode,1)
  await client.query(`UPDATE users SET reset_code = $1 WHERE email = $2`, [resetCode, email])
  res.redirect('/?verify=Please check your email to reset password')
})

registerRoutes.post('/editform', async (req, res)=>{
  // console.log(123)
  let email = req.body.email;
  let username = req.body.username;
  let password = req.body.password;
  let oldpassword = req.body.oldPassword;
  let passwordRepeat = req.body.passwordRepeat;
  let emailError = "";
  let usernameError = "";
  let passwordRepeatError = "";
  let passwordError = "";
  let regUsername = /^[a-zA-Z0-9]{8,255}$/;
  let regEmail = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
  let regPassword = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,255}/;
  //double check password

  let oldpw = await client.query(`SELECT * FROM users where id = ${req.session?.user.id}`)
  let checkOldPw = await checkPassword(oldpassword, oldpw.rows[0].password)
  // console.log(checkOldPw)
  if(!checkOldPw&&!(oldpassword==="")){
    res.json({success:false, oldPasswordError:`Doesn't match previous password, please use forget password`});
    return
  }

  if(password&&!oldpassword){
    res.json({success:false, oldPasswordError:`Doesn't match previous password, please use forget password`});
    return
  }

  if (password != passwordRepeat) {
    passwordRepeatError = "Password don't match"
  }
  if (!regEmail.test(email)&&!(email==="")) {
    emailError = "Email is not valid";
    // throw new Error("Email is invalid");
  }

  if (!regUsername.test(username)&&!(username==="")) {
    usernameError = "Username doesn't match the required pattern";
  }

  if (!regPassword.test(password)&&!(password==="")) {
    passwordError = "Password doesn't match the required pattern"
  }


  let emailResult = await client.query(`SELECT * FROM users where email = $1`, [email])
  if (emailResult.rows[0] && !emailError) {
    emailError = "Email has been registered"
  }
  
  if (!emailError && !passwordError && !passwordRepeatError && !usernameError) {
    if(email){
    const activate_code= uuidv4();
    let ver_emailResult = await client.query(`SELECT * FROM users where update_email = $1`, [email])
    if (ver_emailResult.rows[0]){
      await client.query(`UPDATE users SET activate_code = null,update_email= null where id =$1`, [ver_emailResult.rows[0].id])
    }
    verifyEmail(email, activate_code,0)
      await client.query(`UPDATE users SET activate_code=$1, update_email= $2 where id = ${req.session?.user.id}`, [activate_code, email]);
    }
    if(username){
       await client.query(`UPDATE users SET username=$1 where id = ${req.session?.user.id}`, [username]);
       if (req.session) {
        req.session.user.username = username}
    }

    if(password){
      const userHashPw = await hashPassword(password)
      await client.query(`UPDATE users SET password=$1 where id = ${req.session?.user.id}`, [userHashPw]);
      req.session?.destroy(function (err) {
        console.log(err)
      })
      res.json({redirect:true})
      return
    }
    res.json({success:true})
    // if (req.session) {
    //   req.session.user = {
    //     id: newUser.rows[0].id,
    //     username: newUser.rows[0].username,
    //     email: newUser.rows[0].email
    //   }
    // }
    // res.redirect('/setup-character.html')
    return
  }
  res.json({success:false, emailError:emailError,passwordError:passwordError, usernameError: usernameError,passwordRepeatError:passwordRepeatError});
})

registerRoutes.post('/register', async (req, res) => {
  let email = req.body.emailField;
  let username = req.body.usernameField;
  let password = req.body.passwordField;
  let passwordRepeat = req.body.passwordRepeatField;
  let emailError = "";
  let usernameError = "";
  let passwordRepeatError = "";
  let passwordError = "";
  let regUsername = /^[a-zA-Z0-9]{8,255}$/;
  let regEmail = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
  let regPassword = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,255}/;
  //double check password
  if (password != passwordRepeat) {
    passwordRepeatError = "Password don't match"
  }
  if (!regEmail.test(email)) {
    emailError = "Email is not valid";
  }

  if (!regUsername.test(username)) {
    usernameError = "Username doesn't match the required pattern";
  }

  if (!regPassword.test(password)) {
    passwordError = "Password doesn't match the required pattern"
  }
  try {

    // check email unique 
    let emailResult = await client.query(`SELECT * FROM users where email = $1`, [email])
    if (emailResult.rows[0] && !emailError) {
      emailError = "Email has been registered"
    }

    if (!emailError && !passwordError && !passwordRepeatError && !usernameError) {
      const userHashPw = await hashPassword(password)
      const activate_code= uuidv4();
      verifyEmail(email, activate_code,0)
      const newUser = await client.query(`INSERT INTO users(username,email,password,is_active,activate_code) VALUES ($1,$2,$3,false,$4) RETURNING *`, [username, email, userHashPw, activate_code])
      console.log(newUser)
      res.redirect('/?verify=Please check your email for verification')
      // if (req.session) {
      //   req.session.user = {
      //     id: newUser.rows[0].id,
      //     username: newUser.rows[0].username,
      //     email: newUser.rows[0].email
      //   }
      // }
      // res.redirect('/setup-character.html')
      return
    }
  }
  catch (error) {
    console.log("error", error)
    // Need to handle error gracefully
    res.end("An error occur", error)
    return
  }
  res.redirect(`/register.html?passwordFieldError=${passwordError}&usernameFieldError=${usernameError}&passwordRepeatField=${passwordRepeatError}&emailFieldError=${emailError}`);
})