import express from 'express';
import expressSession from 'express-session';
import bodyParser from "body-Parser";
import dotenv from "dotenv";
import { isLoggedIn } from './guardsRoutes';
import grant from 'grant-express';
import multer from 'multer';
// import path from "path"
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `${__dirname}/private/userImage`);
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
})
const upload = multer({storage})


dotenv.config();

import { Client } from "pg";
import { loginRoutes } from './loginRoutes';
import { registerRoutes } from './registerRoutes';
import { setupRoutes } from './set-upRoutes';
import { walletRoutes } from './walletRoutes';
import { reportRoutes } from './reportRoutes'
import { memorialRoutes } from './memorialRoutes'
import { isInit } from './isInitRoutes.ts'

export let client = new Client({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
});

// connect to server, test function, should return hello world in console
client.connect()

// basic settings
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressSession({
  secret: 'Save more money with furry wallet',
  resave: true,
  saveUninitialized: true
}));

// app.use("/",(req,res,next)=>{
//   console.log(123)
//   next()
// })
//Google OAuth
app.use(grant({
  "defaults": {
    "protocol": "http",
    "host": "localhost:8080",
    "transport": "session",
    "state": true,
  },
  "google": {
    "key": process.env.GOOGLE_CLIENT_ID || "",
    "secret": process.env.GOOGLE_CLIENT_SECRET || "",
    "scope": ["profile", "email"],
    "callback": "/login/google"
  },

  // let appid= "622252318490466";
  // let appsecret = "873636b875c144f9f3afc2d423d829b9";

  "facebook": {
    "key": process.env.FACEBOOK_CLIENT_ID || "",
    "secret": process.env.FACEBOOK_CLIENT_SECRET || "",
    "scope": ["email"],
    "callback": "/login/facebook"
  }
}));

app.post('/upload',upload.single('profile'),async (req,res)=>{
  // console.log(req.file.filename)
   await client.query(`UPDATE users set profile_picture =$1 where id = $2 `, [req.file.filename,req.session?.user.id]); // get the name of the file
   // Other fields are in req.body
   if(req.session){
   req.session.user.picture = req.file.filename}
  res.redirect('/useredit.html');
})

app.get('/petinfo', async (req, res) => {
  let result = await client.query('Select * from adoption WHERE user.id = $1', [req.session?.user.id])
  res.json(result.rows)
})

app.get('/personalinfo', async (req, res)=>{
  if(req.session?.user?.username){
    res.json({username:req.session?.user?.username, email:req.session?.user?.email, picture:req.session?.user?.picture});
    return
  }
  res.json({success:false});
})

app.use('/', loginRoutes);
app.use('/', registerRoutes);
app.use('/', setupRoutes);
app.use('/', walletRoutes);
app.use('/', reportRoutes);
app.use('/', memorialRoutes);

// public files

app.use('/setup-character.html', isLoggedIn, isInit, (req, res, next) => {
  next()
})

app.get('/resetpassword.html', async (req,res,next)=>{
  let code =req.query.code
  // console.log(code)
  if(!code){
    res.redirect('404.html')
    return
  }
  let result = await client.query(`SELECT * FROM users where reset_code = $1`, [code])
  let user = result.rows[0]
  if(!user){
    res.redirect('404.html')
    return
  }
  next()
})
app.use(express.static('public'));
// isInit may not be useful
app.use(isLoggedIn, isInit, express.static('private'));
app.use(express.static('icon'));
app.use(express.static('character-img'));
app.use((req, res) => {
  res.redirect('404.html')
});
// set up the server
const PORT = 8080;

app.listen(PORT, () => {
  console.log(`server run at ${PORT}`);
});


