CREATE DATABASE furry_wallet;

CREATE TABLE users(
    id SERIAL primary key,
    username VARCHAR(255) not null,
    email VARCHAR(255) not null,
    password VARCHAR(255) not null,
    google_id VARCHAR(255)
);

CREATE TABLE pets(
    id SERIAL primary key,
    name VARCHAR(255) not null,
    color VARCHAR(255) not null
);

CREATE TABLE adoption(
    id SERIAL primary key,
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id),
    pet_id INTEGER,
    FOREIGN KEY (pet_id) REFERENCES pets(id),
    hp INTEGER not null,
    hungry INTEGER not null,
    name VARCHAR(255) not null,
    color VARCHAR(255) not null,
    sex VARCHAR(10) not null
);

CREATE TABLE categories(
    id SERIAL primary key,
    name VARCHAR(255) not null,
    image VARCHAR(255) not null
);


CREATE TABLE transactions(
    id SERIAL primary key,
    category_id INTEGER,
    FOREIGN KEY (category_id) REFERENCES categories(id),
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id),
    date_created TIMESTAMP not null,
    amount DECIMAL(15,2) not null,
    is_expense BOOLEAN not null
);

CREATE TABLE props(
    id SERIAL primary key,
    name VARCHAR(255) not null,
    image VARCHAR(255) not null
);


CREATE TABLE transaction_prop(
    id SERIAL primary key,
    transaction_id INTEGER not null,
    FOREIGN KEY (transaction_id) REFERENCES transactions(id),
    prop_id INTEGER not null,
    FOREIGN KEY (prop_id) REFERENCES props(id),
    adoption_id INTEGER not null,
    FOREIGN KEY (adoption_id) REFERENCES adoption(id)
);

-- ALTER TABLE categories ADD COLUMN image VARCHAR(255) not null;
-- ALTER TABLE users ADD COLUMN email VARCHAR(255) not null;

ALTER TABLE pets DROP COLUMN color;
ALTER TABLE pets ADD COLUMN image VARCHAR(255) not null;


INSERT into categories (name, image) VALUES ('Food', 'utensils-solid.svg'),
('Drink', 'coffee-solid.svg'),
('Transport', 'bus-solid.svg'),
('Shopping', 'shopping-bag-solid.svg'),
('Entertainment', 'gamepad-solid.svg'),
('Housing', 'home-solid.svg'),
('Digital', 'mobile-alt-solid.svg'),
('Medical', 'plus-solid.svg'),
('Misc.', 'tag-solid.svg'),
('Learning', 'book-solid.svg'),
('Social', 'user-friends-solid.svg'),
('Income', 'piggy-bank-solid.svg'),
('Groceries', 'shopping-cart-solid.svg'),
('Clothing', 'tshirt-solid.svg'),
('Insurance', 'car-crash-solid.svg'),
('Tax', 'envelope-solid.svg'),
('Edit', 'heart-solid.svg');

INSERT into users(username, email, password) VALUES ('test', 'test@gmail.com', '123456');

UPDATE categories SET image = 'envelope-solid.svg' where id=16;
ALTER TABLE users ALTER COLUMN password DROP NOT NULL;
ALTER TABLE transactions ADD COLUMN transaction_date timestamp not null; 
ALTER TABLE adoption ADD COLUMN born_date timestamp not null;
ALTER TABLE adoption ADD COLUMN condition_update timestamp not null;
ALTER TABLE adoption ADD COLUMN is_alive boolean not null;
ALTER TABLE users ADD COLUMN facebook_id VARCHAR(255);

ALTER TABLE users ADD COLUMN is_active boolean;
ALTER TABLE users ADD COLUMN activate_code VARCHAR(255);

ALTER TABLE users ADD COLUMN coins INTEGER;

INSERT INTO props (name,image) VALUES ('canned food','canned-food.svg');
INSERT INTO props (name,image) VALUES ('dried food','pet-bowl.svg');
INSERT INTO props (name,image) VALUES ('water','bowl.svg');

CREATE TABLE user_prop (
    id SERIAL primary key,
    user_id INTEGER not null,
    FOREIGN KEY (user_id) REFERENCES users(id),
    prop_id INTEGER not null,
    FOREIGN KEY (prop_id) REFERENCES props(id)
);

ALTER TABLE user_prop ADD COLUMN date_created timestamp;

ALTER TABLE users ADD COLUMN reset_code VARCHAR(255);
UPDATE categories SET name = 'Others' where id = 17;

ALTER TABLE users ADD COLUMN profile_picture text;
ALTER TABLE users ADD COLUMN update_email VARCHAR(255);