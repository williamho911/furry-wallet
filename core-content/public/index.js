
window.onload = function checker() {
    let searchURL = window.location.search;
    const urlParams = new URLSearchParams(searchURL);

    if (urlParams.get('verify')===""){
        document.querySelector("#notice-msg").innerHTML = "Empty verification code";
        document.querySelector(".notice-display").classList.remove("hidden")
    }

    if (urlParams.get('verify')) {
        document.querySelector("#notice-msg").innerHTML = urlParams.get('verify');
        document.querySelector(".notice-display").classList.remove("hidden")
    }

    if (urlParams.get('password')) {
        document.querySelector("#passwordError").innerHTML = urlParams.get('password');
    }
    loadUser()
}

document.querySelector('.msgButton').addEventListener(
    'click', (e) => {
        document.querySelector('.notice-display').classList.add('hidden')
    }
)

$(function () { // a self calling function
    function onScrollInit(items, trigger) { // a custom made function
        items.each(function () { //for every element in items run function
            var osElement = $(this), //set osElement to the current
                osAnimationClass = osElement.attr('data-animation'), //get value of attribute data-animation type
                osAnimationDelay = osElement.attr('data-delay'); //get value of attribute data-delay time

            osElement.css({ //change css of element
                '-webkit-animation-delay': osAnimationDelay, //for safari browsers
                '-moz-animation-delay': osAnimationDelay, //for mozilla browsers
                'animation-delay': osAnimationDelay //normal
            });

            var osTrigger = (trigger) ? trigger : osElement; //if trigger is present, set it to osTrigger. Else set osElement to osTrigger

            osTrigger.waypoint(function () { //scroll upwards and downwards
                osElement.addClass('animated').addClass(osAnimationClass); //add animated and the data-animation class to the element.
            }, {
                triggerOnce: true, //only once this animation should happen
                offset: '80%' // animation should happen when the element is 70% below from the top of the browser window
            });
        });
    }

    onScrollInit($('.os-animation')); //function call with only items
    onScrollInit($('.staggered-animation'), $('.staggered-animation-container')); //function call with items and trigger
});

async function loadUser() {
    const res = await fetch('/personalinfo', {
        method: "get",
    });
    const result = await res.json();
    if (result.username) {
        document.querySelector("#navbarSupportedContent").innerHTML = `
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="#aboutUs">About us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#contact">Contact us</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="/wallet.html" id="usernameDiv"></a>
            </li>
        </ul>   
        <a href="/logout" class="btn btn-success" type="submit">Logout</a>
    `
        document.querySelector(".card-body").innerHTML = `
                <h2>Welcome Back</h2>
                <h2>${result.username}</h2>
                <a href="/wallet.html" class="btn btn-success">Press here to start</a>
                <a href="/logout" class="btn btn-success" type="submit">Logout</a>
            `;
        if(result.picture){
            document.querySelector("#usernameDiv").innerHTML = `<img src="/userImage/${result.picture}" alt="userpicture"  height="30px" width="30px"> ${result.username}`
        }else{
            document.querySelector("#usernameDiv").innerHTML = `<img src="/userImage/default.png" alt="userpicture"  height="30px" width="30px"> ${result.username}`
        }
    }

    console.log(result)
}

