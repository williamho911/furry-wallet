import express, {Request, Response} from 'express';
import { client } from './main';

export const memorialRoutes = express.Router();
memorialRoutes.get('/memorial', getMemorial)

async function getMemorial(req:Request, res:Response){
    const result = await client.query(`SELECT * FROM adoption WHERE user_id = $1 and is_alive = 'f'`, [req.session?.user.id])
    res.json(result.rows)
}
