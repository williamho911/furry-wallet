import express, { Request, Response } from 'express';
import { client } from './main';
import { to_csv, json_to_csv } from "@beenotung/tslib/csv";
// import format from "date-fns/format"

export const reportRoutes = express.Router()

// reportRoutes.get("/", async (req, res) => {
//     res.redirect('report.html')
// })

reportRoutes.get("/report", getTransactionData)
reportRoutes.get("/report.csv", genTransactionReport)
reportRoutes.get("/dailyExpense", genDailyTotalExpense)


export async function getTransactionData(req: Request, res: Response) {
  // Need to validate date
  let dateFrom = new Date(req.query.dateFrom +"");
  let dateTo = new Date(req.query.dateTo+"");

  if(isNaN(dateFrom.getTime())|| isNaN(dateTo.getTime())){
    res.status(400).json({msg:"dateFrom/dateTo not in date format"});
    return;
  }
  // console.log(req.query)
  // console.log('From date: ', dateFrom)
  // console.log('To day: ', dateTo)
  let result = await client.query(`SELECT image, name, SUM(amount) FROM categories INNER JOIN transactions ON categories.id = category_id WHERE user_id = $3 AND DATE(date_created) BETWEEN $1 AND $2 GROUP BY category_id, name, image ORDER BY sum DESC;`, [dateFrom, dateTo, parseInt(req.session?.user.id)])
  res.json(result.rows)
}

export async function genTransactionReport(req: Request, res: Response) {
  let dateFrom = req.query.dateFrom
  let dateTo = req.query.dateTo
  let result = await client.query(`SELECT name, SUM(amount) FROM categories INNER JOIN transactions ON categories.id = category_id WHERE user_id = $3 AND DATE(date_created) BETWEEN $1 AND $2 GROUP BY category_id, name ORDER BY sum DESC;`, [dateFrom, dateTo, parseInt(req.session?.user.id)])

  let rows = json_to_csv(result.rows)
  let csv = to_csv(rows)

  res.attachment(`Expense_report(${dateFrom}to${dateTo}).csv`)
  res.status(200).send(csv)
}

export async function genDailyTotalExpense(req: Request, res: Response) {
  let dateFrom = req.query.dateFrom
  let dateTo = req.query.dateTo
  // console.log(dateFrom)
  // console.log(dateTo )
  let result = await client.query(`SELECT SUM(amount), DATE(transaction_date) FROM transactions WHERE user_id = $3 AND DATE(transaction_date) BETWEEN $1 AND $2 GROUP BY transaction_date;`, [dateFrom, dateTo, parseInt(req.session?.user.id)])
  res.json(result.rows)
  // console.log("Result: ", result.rows[0].date)
}