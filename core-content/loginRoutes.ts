import express, { Request, Response } from 'express';
import { client } from './main';
import { hashPassword,checkPassword } from './hashRoutes';
import fetch from 'node-fetch'
// import grant from 'grant-express';

export const loginRoutes = express.Router();
loginRoutes.get('/verify', verifyAccount)
loginRoutes.get('/login/google', loginGoogle);
loginRoutes.get('/login/facebook', loginFacebook);
loginRoutes.post('/login', login);
loginRoutes.get('/logout', logout);


async function verifyAccount (req:Request, res:Response){
    // console.log(req.query.code)
    let verifyCode = req.query.code;
    if(!verifyCode){
        res.redirect('/?verify=')
        return
    }
    let result = await client.query(`SELECT * FROM users where activate_code = $1`, [verifyCode])
    let user = result.rows[0]
    if (!user){
        res.redirect('/?verify=Invalid activation code, please try again')
        return
    }
    if(user){
        if(user.update_email){
            await client.query(`UPDATE users SET email = $1, activate_code = null, update_email = null WHERE email = $2`, [user.update_email, user.email])
        }else{
            await client.query(`UPDATE users SET is_active = true,activate_code = null WHERE email = $1`, [user.email])
            // await client.query(`UPDATE users SET  WHERE email = $1`,[user.email])
        }
    }
    // Force login
    req.session?.destroy(function (err) {
        console.log(err)
    })
    res.redirect('/?verify=Your account has been activated, please login and have fun!')
}

// login post request handler
async function login(req: Request, res: Response) {
    let email = req.body.email
    let password = req.body.password
    let remember = req.body.remember

    try {
        let result = await client.query(`SELECT * FROM users where email = $1`, [email])
        if (result.rows[0]) {
            if(!result.rows[0].is_active){
                res.redirect('/?verify=Your account has not been verified')
                return
            }
            let check = await checkPassword(password, result.rows[0].password)
            if (check) {
                if (req.session) {
                    req.session.user = {
                        id: result.rows[0].id,
                        username: result.rows[0].username,
                        email: result.rows[0].email,
                        picture: result.rows[0].profile_picture
                    };
                    if (remember) { req.session.cookie.maxAge = 2592000000; }
                }
                return res.redirect('/wallet.html')
            }else{
                res.redirect("/?password=incorrect username or password");
                return;
            }
        }
    } catch (error) {
        res.redirect("/?password=Error Occurred");
        return;
    }
    // res.redirect("/?password=incorrect username or password");
    // return
};


async function logout(req: Request, res: Response) {
    if (req.session) {
        req.session.destroy(function (err) {
            console.log(err)
        })
    }
    res.redirect('/')
}


//Google OAuth
async function loginGoogle(req: express.Request, res: express.Response) {
    const accessToken = req.session?.grant.response.access_token;
    const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
        method: "get",
        headers: {
            "Authorization": `Bearer ${accessToken}`
        }
    });
    const result = await fetchRes.json();
    // console.log(result)
    const users = (await client.query(`SELECT * FROM users WHERE users.email = $1`, [result.email])).rows;
    const user = users[0];
    
    if (!user) {
        var randomString = Math.random().toString(36).slice(-8);
        const userHashPw = await hashPassword(randomString)
        const newUser = await client.query(`INSERT INTO users (username, password, google_id, email,is_active) VALUES ($1, $2, $3, $4, true) RETURNING *`, [result.name, userHashPw, result.id, result.email])
        if (req.session) {
            req.session.user = {
                id: newUser.rows[0].id,
                username: newUser.rows[0].username,
                email: newUser.rows[0].email,
                picture: newUser.rows[0].profile_picture
            }
        }
        return res.redirect('/setup-character.html')
    }

    if(user && !user.google_id){
        await client.query(`UPDATE users SET google_id = $1 where email=$2`, [result.id, result.email])
    }

    if (req.session) {
        req.session.user = {
            id: user.id,
            username: user.username,
            email: user.email,
            picture: user.profile_picture
        };
    }
    return res.redirect('/wallet.html')
}

async function loginFacebook (req:express.Request,res:express.Response){
    const accessToken = req.session?.grant.response?.access_token;
    const fetchRes = await fetch(`https://graph.facebook.com/me?fields=id,name,email&access_token=${accessToken}`,{
        method:"get",
    });

    const result = await fetchRes.json();
    const users = (await client.query(`SELECT * FROM users WHERE users.email = $1`, [result.email])).rows;
    const user = users[0];
    // console.log(user)
    if (!user) {
        var randomString = Math.random().toString(36).slice(-8);
        const userHashPw = await hashPassword(randomString)
        const newUser = await client.query(`INSERT INTO users (username, password, facebook_id, email, is_active) VALUES ($1, $2, $3, $4, true) RETURNING *`, [result.name, userHashPw, result.id, result.email])
        if (req.session) {
            req.session.user = {
                id: newUser.rows[0].id,
                username: newUser.rows[0].username,
                email: newUser.rows[0].email,
                picture: newUser.rows[0].profile_picture
            }
        }
        return res.redirect('/setup-character.html')
    }

    if(user && !user.facebook_id){
        await client.query(`UPDATE users SET facebook_id = $1 where email=$2`, [result.id, result.email])
    }

    if (req.session) {
        req.session.user = {
            id: user.id,
            username: user.username,
            email: user.email,
            picture: user.profile_picture
        };
    }
    // console.log(result.name)
    // console.log(req.session?.user.id)
    return res.redirect('/wallet.html')
}