

function updatePet(picker, id) {

  let parent = document.getElementById('char' +  id)
  // let parent = picker.targetElement.parentNode.parentNode.parentNode
  // let saveColor = document.getElementById('js-color-' + id).style.backgroundImage
  // console.log(saveColor)

  const petFurry = parent.querySelectorAll(".furry-color");
  for (let i = 0; i < petFurry.length; i++) {
    petFurry[i].style.fill = picker.toHEXString();
  }
}

// function savedColor(id) {
//   let parent = document.getElementById('char' +  id)
//   let colorChanged = parent.getElementsByClassName('furry-color')
//   return colorChanged[0].style.cssText
// }

let chars = document.getElementsByClassName("character")
for (let i = 0; i < chars.length; i++) {
  chars[i].addEventListener("click", async () => {
    let id = i + 1

    let current = document.getElementsByClassName("active-pet");
    if (current.length > 0) {
      current[0].className = current[0].className.replace(" active-pet", "");
    }
    chars[i].className += await " active-pet";

    // console.log(current[0)
    // console.log(savedColor(id))
  })
}

// async function chosenPet() {
//   let checkActive = document.querySelector("character-box");
//   console.log(checkActive.classList.contains("active-pet"))
// }
// chosenPet()

// let current = document.getElementsByClassName("active-pet");

let submit = document.querySelector('#choosePet')
submit.addEventListener('submit', async (event) => {
  event.preventDefault()
  let chosenPet = document.querySelector(".active-pet")
  let colorChanged = chosenPet.getElementsByClassName('furry-color')
  let saveColor = colorChanged[0].style.cssText.slice(6, -1)
  let inputName = document.querySelector('#pet-name').value
  console.log(colorChanged[0].style.cssText)

   const res = await fetch(`/main/${chosenPet.dataset.animal}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: JSON.stringify ({
      color: saveColor,
      name: inputName
    })
  })
  let result = await res.json() 
  if (result == "done"){
    window.location="http://localhost:8080/wallet.html";
  }
})



// const svg = document.createElementNS("../../character-img/01-cat.svg", svg)

async function loadUser() {
  const res = await fetch('/personalinfo', {
      method: "get",
  });
  const result = await res.json();
  if (result.username) {
      document.querySelector("#navbarSupportedContent").innerHTML = `
      <ul class="navbar-nav ml-auto">
      <li class="nav-item">
          <a class="nav-link" href="/wallet.html" id="usernameDiv"></a>
      </li>
      </ul>   
      <a href="/logout" class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</a>
  `
      if(result.picture){
          document.querySelector("#usernameDiv").innerHTML = `<img src="/userImage/${result.picture}" alt="userpicture"  height="30px" width="30px"> ${result.username}`
      }else{
          document.querySelector("#usernameDiv").innerHTML = `<img src="/userImage/default.png" alt="userpicture"  height="30px" width="30px"> ${result.username}`
      }
  }

  console.log(result)
}

loadUser()
