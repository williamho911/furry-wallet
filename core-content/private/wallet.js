
// function stripHtml(html) {
//     var tmp = document.createElement("DIV");
//     tmp.innerHTML = html;
//     return tmp.innerText;
// }



//Assign active class for pressed button
let buttons = document.querySelectorAll('.button')
for (let button of buttons) {
    button.addEventListener('click', (event) => {
        if (document.querySelector(".active-button") == null) {
            button.classList.add('active-button')
        } else {
            var current = document.getElementsByClassName("active-button");
            current[0].className = current[0].className.replace("active-button", "");
            button.classList.add('active-button')
        }
    })
}

const tableContent = document.querySelector('.record')
const coins = document.querySelector('.amount')
const tableCoins = document.querySelector('.table-coin')
// Get user info
let name = ""
async function getInfo() {
    const info = await fetch('/info')
    let result = await info.json()
    name = result[0].username
    money = result[0].coins
    coins.innerHTML = money
    tableCoins.innerHTML = `<img src="/props-img/money.svg" width="20px">   ${money}`

}

// periodic update fetch from server to update petState;
// Single source of truth
let petState;

// Periodic update fetch
let coins; 

//Location to write pet data
const pet = document.querySelector('.pet')
const condition = document.querySelector('.health')
//Feed pet
async function feed() {
    const dialogs = ["Yeah", "FOOD!", "Finally"]
    const dialog = dialogs[Math.floor(Math.random() * 3)]
    const res = await fetch("/pet")
    const info = await res.json()
    // const petState = await res.json();
    const petCondition = info[0]
    let color = petCondition.color
    let alive = petCondition.is_alive
    let id = petCondition.pet_id
    let hp = petCondition.hp
    let hunger = petCondition.hungry
    let petName = petCondition.name
    console.log(alive + "--" + id)
    condition.innerHTML = `<div class="pet-name">${petName}</div>
    <hr class="solid">
    <div>Health</div>
    <div class="progress">
        <div class="progress-bar bg-info" role="progressbar" style="width: ${hp}%" aria-valuenow="${hp}"
            aria-valuemin="0" aria-valuemax="100"></div>
    </div>
    <div>Hunger</div>
    <div class="progress">
        <div class="progress-bar bg-danger" role="progressbar" style="width: ${hunger}%" aria-valuenow="${hunger}"
            aria-valuemin="0" aria-valuemax="100"></div>
    </div>`

    if (alive) {
        if (id == 1) {
            let result = await fetch("character-img/01-cat-happy.svg")
            let catSVGHappy = await result.text()
            console.log(catSVGHappy)
            pet.innerHTML = `
                        <div class="pet-img">
                            ${catSVGHappy}
                            <div class="dialogue">
                            ${dialog}
                            </div>
                        </div>
                    `
        } else {
            let result = await fetch("character-img/02-dog-happy.svg")
            let dogSVGHappy = await result.text()
            console.log(dogSVGHappy)
            pet.innerHTML = `
                            <div class="pet-img">
                                ${dogSVGHappy}
                                <div class="dialogue">
                                ${dialog}
                                </div>
                            </div>
                        `
        }
    }
    const petFurry = document.querySelectorAll(".furry-color");
    for (let i = 0; i < petFurry.length; i++) {
        petFurry[i].style.fill = color;
    }
}


//Submit transaction
let submit = document.querySelector("#moneyInput")
submit.addEventListener('submit', async (event) => {
    event.preventDefault()
    let date = document.querySelector('input[type="date"]').value
    let current = document.querySelector(".active-button");
    let amount = document.querySelector('.expense').value
    if (current) {
        await fetch('/submit/' + current.dataset.item, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                content: amount,
                date: date
            })
        })
        document.querySelector('.message').innerHTML = ""
        await document.querySelector('#moneyInput').reset()
        passDate()
        // feed()
    } else {
        document.querySelector('.message').innerHTML = `<p>Choose the category before you feed me</p>`;
    }

    getInfo()
})

//Delete & edit transaction
document.querySelector('.record').addEventListener('click', async (event) => {
    if (event.target.matches('.delete-icon')) {
        await fetch('/submit/' + event.target.dataset.delete, {
            method: 'DELETE'
        })
        passDate()
    } else if (event.target.matches('.edit-icon')) {
        // let table = document.querySelector('#summary')
        let num = event.target.dataset.delete
        let row = document.querySelector(`#input-box${event.target.dataset.delete}`)
        row.innerHTML = `<input type="number" value="${row.innerHTML}">`
        let act = document.querySelector('.record input')
        act.focus()
        act.addEventListener('blur', async () => {
            await fetch('/submit/' + num, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                },
                body: JSON.stringify({
                    value: act.value
                })
            })
            passDate()
        })
    }
})
//Get today's date as default for loading transactions
function getDate() {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    document.querySelector('input[type="date"]').value = today;
}

//Show transactions by specified date
async function passDate() {
    let date = document.querySelector('input[type="date"]').value
    await fetch('/categories?date=' + date, {
        method: 'GET'
    })
    let res = await fetch('/categories?date=' + date)
    let transactions = await res.json()
    let result = ""
    tableContent.innerHTML = ""
    for (let transaction of transactions) {
        if (transaction.is_expense) {
            result += `                                
                <tr>
                    <th scope="row"><img src="${transaction.image}"></th>
                    <td>${transaction.name}</td>
                    <td><div id="input-box${transaction.id}" class="expense">${transaction.amount}</div></td>
                    <td><div class="delete-icon" data-delete=${transaction.id}><i class="fas fa-times"></i></div></td>
                    <td><div class="edit-icon" data-delete=${transaction.id}><i class="fas fa-pen"></i></div></td>
                </tr>                  
        `
        } else {
            result += `                                  

                <tr>
                    <th scope="row"><img src="${transaction.image}"></th>
                    <td>${transaction.name}</td>
                    <td><div id="input-box${transaction.id}" class="income">${transaction.amount}</div></td>
                    <td><div class="delete-icon" data-delete=${transaction.id}><i class="fas fa-times"></i></div></td>
                    <td><div class="edit-icon" data-delete=${transaction.id}><i class="fas fa-pen"></i></div></td>
                </tr>                  
                
        `
        }
    }
    tableContent.innerHTML = `    
    <table class="table" id="summary">
    <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col">Category</th>
            <th scope="col">HKD</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
        ${result}
    </tbody>
    </table>`

    // loadPet();
}
//Button to change date
document.querySelector('.date-select').addEventListener('click', (event) => {
    let date = document.querySelector('input[type="date"]').value
    let newDate = new Date(date)
    if (event.target.matches('.arrow-left')) {
        newDate.setDate(newDate.getDate() - 1);
    } else if (event.target.matches('.arrow-right')) {
        newDate.setDate(newDate.getDate() + 1);
    }
    let dd = newDate.getDate();
    let mm = newDate.getMonth() + 1;
    let yyyy = newDate.getFullYear();
    if (dd < 10) { // padding
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    let day = yyyy + '-' + mm + '-' + dd;
    document.querySelector('input[type="date"]').value = day
    passDate()
})


async function loadPet() {
    //Dialog
    let catGreetings = ["I just broke the vase", "Meow", "Fish?"]
    let catGreeting = catGreetings[Math.floor(Math.random() * 3)]
    let dogGreetings = ["Play with me!", "Woof", "Meal time!?"]
    let dogGreeting = dogGreetings[Math.floor(Math.random() * 3)]
    let hungrySignals = ['No food for a long time', "Canned food now!", "Some food now?"]
    let hungrySignal = hungrySignals[Math.floor(Math.random() * 3)]
    let dyingSignals = ['Starving', 'Seems here is my end', 'My lfe is flashing before my eyes']
    let dyingSignal = dyingSignals[Math.floor(Math.random() * 3)]

    // Can get from petState
    const res = await fetch("/pet")
    const info = await res.json()
    console.log(info)
    const petCondition = info[0]
    const hp = parseInt(petCondition.hp)
    const hunger = parseInt(petCondition.hungry)
    const color = petCondition.color
    const name = petCondition.name
    console.log(name)
    const petName = document.querySelector('.pet-name')
    condition.innerHTML = `<div class="pet-name">${name}</div>
    <hr class="solid">
    <div>Health</div>
    <div class="progress">
        <div class="progress-bar bg-info" role="progressbar" style="width: ${hp}%" aria-valuenow="${hp}"
            aria-valuemin="0" aria-valuemax="100"></div>
    </div>
    <div>Hunger</div>
    <div class="progress">
        <div class="progress-bar bg-danger" role="progressbar" style="width: ${hunger}%" aria-valuenow="${hunger}"
            aria-valuemin="0" aria-valuemax="100"></div>
    </div>`
    if (petCondition.pet_id == 1) {
        if (hunger >= 70) {
            if (hp > 50) {
                let result = await fetch("character-img/01-cat-angry.svg")
                let catSVGAngry = await result.text()
                pet.innerHTML = `
                <div class="pet-img">
                    ${catSVGAngry}
                    <div class="dialogue">
                        ${hungrySignal}
                    </div>
                </div>
            `
            }
            if (hp <= 50) {
                let result = await fetch("character-img/01-cat-diesoon.svg")
                let catSVGDiesoon = await result.text()
                pet.innerHTML = `
                    <div class="pet-img">
                        ${catSVGDiesoon}
                        <div class="dialogue">
                            ${dyingSignal}
                        </div>
                    </div>
                `
                if (hp == 0) {
                    pet.innerHTML = `
                    <div class="pet-grave">
                    <img src="character-img/03-grave.svg">
                    </div>
                    `
                    petName.innerHTML = name
                    document.querySelector(".reset-but").style.display = "inline"
                    getInfo()
                    clearInterval(timer)
                    getInfo()
                }
            }
        } else {
            let result = await fetch("character-img/01-cat.svg")
            let catSVG = await result.text()
            pet.innerHTML = ``
            pet.innerHTML = `
                <div class="pet-img">
                    ${catSVG}
                    <div class="dialogue">
                        ${catGreeting}
                    </div>
                </div>
            `
        }
    } else {
        if (hunger >= 70) {
            if (hp > 50) {
                let result = await fetch("character-img/02-dog-angry.svg")
                let dogSVGAngry = await result.text()
                pet.innerHTML = `
                    <div class="pet-img">
                        ${dogSVGAngry}
                        <div class="dialogue">
                        ${hungrySignal}
                        </div>
                    </div>
                `
            }
            if (hp <= 50) {
                let result = await fetch("character-img/02-dog-diesoon.svg")
                let dogSVGDiesoon = await result.text()
                pet.innerHTML = `
                    <div class="pet-img">
                        ${dogSVGDiesoon}
                        <div class="dialogue">
                            ${dyingSignal}
                        </div>
                    </div>
                    
                `
                if (hp == 0) {
                    pet.innerHTML = `
                    <div class="pet-grave">
                    <img src="character-img/03-grave.svg">
                    </div>
                    `
                    petName.innerHTML = name
                    getInfo()
                    clearInterval(timer)
                    document.querySelector(".reset-but").style.display = "inline"
                }
            }
        } else {
            let result = await fetch("character-img/02-dog.svg")
            let dogSVG = await result.text()
            pet.innerHTML = `
                <div class="pet-img">
                    ${dogSVG}
                    <div class="dialogue">
                        ${dogGreeting}
                    </div>
                </div>
                
            `
        }
    }

    const petFurry = document.querySelectorAll(".furry-color");
    for (let i = 0; i < petFurry.length; i++) {
        petFurry[i].style.fill = color;
    }
}


//Buy props and feed
// document.querySelector('.dropdown-menu').addEventListener('click', async (event) => {
// })
document.querySelector('.dropdown-menu').addEventListener('click', async (event) => {
    event.stopPropagation();
    event.preventDefault()
    const info = await fetch('/info')
    let result = await info.json()
    money = result[0].coins
    if (event.target.matches('.water')) {
        if (money >= 10) {
            money -= 10
            await fetch('/buy?prop=' + 1, {
                method: 'GET'
            })
        } else {
            alert("You have no enough money")
        }
    } else if (event.target.matches('.dried-food')) {
        console.log('Dried')
        if (money >= 20) {
            money -= 20
            await fetch('/buy?prop=' + 2, {
                method: 'GET'
            })
        } else {
            alert("You have no enough money")
        }
    } else if (event.target.matches('.canned-food')) {
        console.log('Canned')
        if (money >= 50) {
            money -= 50
            await fetch('/buy?prop=' + 3, {
                method: 'GET'
            })
        } else {
            alert("You have no enough money")
        }
    }
    // should be from pet state
    let pets = await fetch(`/pet`)
    let petCondition = await pets.json()
    let isAlive = petCondition[0].is_alive
    if (event.target.matches('.feed-water')) {
        event.preventDefault()
        if (isAlive) {
            await fetch('/feed?item=' + 4, {
                method: 'GET'
            })
            loadPet()
            feed()
        } else {
            alert("It's over...")
        }
    } else if (event.target.matches('.feed-dried')) {
        event.preventDefault()
        if (isAlive){
            await fetch('/feed?item=' + 3, {
                method: 'GET'
            })
            loadPet()
            feed()
        } else {
            alert("It's over...")
        }
    } else if (event.target.matches('.feed-canned')) {
        event.preventDefault()
        if(isAlive){
            await fetch('/feed?item=' + 2, {
                method: 'GET'
            })
            loadPet()
            feed()
        } else {
            alert("It's over...")
        }
    }

    await fetch('/money/' + money, {
        method: 'GET'
    })
    getInfo()
    myBag()
})

const bag = document.querySelector('#bag')
async function myBag() {
    let res = await fetch('/bag')
    let items = await res.json()
    bag.innerHTML = ""
    let output = ""
    for (let item of items) {
        if (item.prop_id == 4) {
            output += `<tr>
            <th scope="row"><img src="/props-img/bowl.svg"></th>
            <td>Water</td>
            <td>${item.count}</td>
            <td><button type="button" class="btn btn-success feed-water">Feed</button></td>                                         
        </tr>`
        } else if (item.prop_id == 3) {
            output += `<tr>
            <th scope="row"><img src="/props-img/pet-bowl.svg"></th>
            <td>Dried food</td>
            <td>${item.count}</td>
            <td><button type="button" class="btn btn-success feed-dried">Feed</button></td>                                         
        </tr>`
        } else {
            output += `<tr>
            <th scope="row"><img src="/props-img/canned-food.svg"></th>
            <td>Canned food</td>
            <td>${item.count}</td>
            <td><button type="button" class="btn btn-success feed-canned">Feed</button></td>                                         
        </tr>`
        }
    }
    bag.innerHTML = `<table class="table" id="summary">                                        
    <tbody>
    
    ${output}
    </tbody>
    </table>
    `
}

let timer = setInterval(async function () { loadPet() }, 5000);
window.onload = function () {
    getDate();
    passDate();
    loadPet();
    myBag();
    getInfo()
};

async function loadUser() {
    const res = await fetch('/personalinfo', {
        method: "get",
    });
    const result = await res.json();
    if (result.username) {
        document.querySelector("#navbarSupportedContent").innerHTML = `
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="#aboutUs">About us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#contact">Contact us</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="/wallet.html" id="usernameDiv"></a>
        </li>
        </ul>   
        <a href="/logout" class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</a>
    `
        if(result.picture){
            document.querySelector("#usernameDiv").innerHTML = `<img src="/userImage/${result.picture}" alt="userpicture"  height="30px" width="30px"> ${result.username}`
        }else{
            document.querySelector("#usernameDiv").innerHTML = `<img src="/userImage/default.png" alt="userpicture"  height="30px" width="30px"> ${result.username}`
        }
    }

    console.log(result)
}

loadUser()