let pethtml = document.querySelector('.pet')

async function loadPet() {
    const res = await fetch("/memorial")
    const pets = await res.json()
    // const petCondition = info[0]
    // color = petCondition.color
    let i = 0


    for (let i = 0; i < pets.length; i++)    
    // for (let pet of pets) 
    {
        let pet =pets[i]
        console.log(pet.color)
        if (pet.pet_id == 1) {

            let result = await fetch("character-img/01-cat.svg")
            let catSVG = await result.text()

            pethtml.innerHTML += `<div class="frame col-lg-4"><div class="pet-img pet${i}"><img class="wreath" src="wreath.svg" width="100px">${catSVG}<div class="petInfo">${pet.name}</div></div></div>`

        } else {
            let result = await fetch("character-img/02-dog.svg")
            let dogSVG = await result.text()
            pethtml.innerHTML += `
            <div class="frame col-lg-4">
            <div class="pet-img pet${i}">
            <img class="wreath" src="wreath.svg" width="100px">
                            ${dogSVG}
                            <div class="petInfo">${pet.name}</div> 
                        </div>
                        </div>               
                    `
        }
        const petFurry = document.querySelectorAll(`.pet${i} .furry-color`);
        for (let i = 0; i < petFurry.length; i++) {
            petFurry[i].style.fill = pet.color;
        }
    }
}
loadPet()

async function loadUser() {
    const res = await fetch('/personalinfo', {
        method: "get",
    });
    const result = await res.json();
    if (result.username) {
        document.querySelector("#navbarSupportedContent").innerHTML = `
        <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="/wallet.html" id="usernameDiv"></a>
        </li>
        </ul>   
        <a href="/logout" class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</a>
    `
        if(result.picture){
            document.querySelector("#usernameDiv").innerHTML = `<img src="/userImage/${result.picture}" alt="userpicture"  height="30px" width="30px"> ${result.username}`
        }else{
            document.querySelector("#usernameDiv").innerHTML = `<img src="/userImage/default.png" alt="userpicture"  height="30px" width="30px"> ${result.username}`
        }
    }

    console.log(result)
}

loadUser()