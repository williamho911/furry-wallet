let password = document.querySelector("#passwordField")
let confirm_password = document.querySelector("#passwordRepeatField");

function validatePassword() {
    if (password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
        document.querySelector("#submit").click();
    } else {
        confirm_password.setCustomValidity('');
    }
}
password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

let submit = document.querySelector('#editForm')
submit.addEventListener('submit', async function (e) {
    console.log(123)
    e.preventDefault();
    document.querySelector("#passwordRepeatFieldError").innerHTML = ``
    const form = this;
    const formObject = {};
    formObject['email'] = form.emailField.value;
    formObject['username'] = form.usernameField.value;
    formObject['password'] = form.passwordField.value;
    formObject['passwordRepeat'] = form.passwordField.value;
    formObject['oldPassword']=form.oldPasswordField.value;
    let checker = false
    console.log(formObject)
    for (let value in formObject) {
        if (formObject[value]) {
            checker = true;
            break
        }
    }

    if (!checker) {
        document.querySelector("#passwordRepeatFieldError").innerHTML = `please fill in one row for submit`;
        return
    }

    const res = await fetch('/editform',{
        method:"POST",
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(formObject)
    });
    const result = await res.json();
    console.log(result)
    if(result.redirect){
        window.location.href = "/?verify=Password changed, Please login again";
    }
    if(!result.success){
        if(result.oldPasswordError){
            document.querySelector("#oldPasswordFieldError").innerHTML =result.oldPasswordError
        }
        if (result.usernameError) {
            document.querySelector("#usernameFieldError").innerHTML = result.usernameError;
        }
        if (result.emailError) {
            document.querySelector("#emailFieldError").innerHTML = result.emailError;
        }
        if (result.passwordRepeatError) {
            document.querySelector("#passwordRepeatFieldError").innerHTML = result.passwordRepeatError;
        }
        if (result.passwordError) {
            document.querySelector("#passwordFieldError").innerHTML = result.passwordError;
        }
    } loadUser()
}
)

async function loadUser() {
    const res = await fetch('/personalinfo', {
        method: "get",
    });
    const result = await res.json();
    console.log(result)
    if (result.username) {
        document.querySelector("#usernamespan").innerHTML = `${result.username}`
        document.querySelector("#emailspan").innerHTML = `${result.email}`
        document.querySelector("#navbarSupportedContent").innerHTML = `
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="#aboutUs">About us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#contact">Contact us</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="/wallet.html" id="usernameDiv"></a>
        </li>
        </ul>   
        <a href="/logout" class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</a>
    `
        if (result.picture) {
            document.querySelector("#usernameDiv").innerHTML = `<img src="/userImage/${result.picture}" alt="userpicture"  height="30px" width="30px"> ${result.username}`
            document.querySelector('.img-div').innerHTML =`<img src="/userImage/${result.picture}" alt="userpicture" class="img-thumbnail">`
        } else {
            document.querySelector("#usernameDiv").innerHTML = `<img src="/userImage/default.png" alt="userpicture"  height="30px" width="30px"> ${result.username}`
            document.querySelector('.img-div').innerHTML =`<img src="/userImage/default.png" alt="userpicture" class="img-thumbnail">`
        }
    }

    console.log(result)
}

loadUser()