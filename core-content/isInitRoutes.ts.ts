import { client } from './main';
import {Request,Response,NextFunction} from 'express';

export async function isInit(req:Request,res:Response,next:NextFunction){
    if(!req.session?.user){
        res.redirect('/')
    }

    if(req.session?.user){
        let id = req.session.user.id;
        // Select by is_alive better
        let pets = (await client.query(`select * from adoption where adoption.user_id = $1  ORDER BY born_date DESC`, [id])).rows
        const pet = pets[0];
        // Should do a better logging  using logger like winston
        // console.log(pet)
        if(pet){
            if(req.originalUrl==="/setup-character.html"&&pet.is_alive){
                res.redirect('/wallet.html'); 
                return;
            }
            next();
            return
        }else{
            const regex = new RegExp(/.html/i);
            let checkHTML = regex.test(req.originalUrl)
            if(req.originalUrl!=='/setup-character.html'&&checkHTML){
                res.redirect('/setup-character.html'); 
                return;
            }
            next();
            return
        }
        
    }
} 